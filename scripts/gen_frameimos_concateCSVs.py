#!/usr/bin/python

import os
import sys
import csv
import subprocess
import re
import urllib
import requests
import time
import glob
import concate_csvs

def gen_content_detailes(original_content):
    #Get detailes of transcoded content using ffprobe
    cmd_Vdetailes = "./ffprobe -v error -select_streams v:0 -show_entries stream=r_frame_rate -of default=noprint_wrappers=1 " + original_content + '> vdetail_log.txt'
    
    print(cmd_Vdetailes)
    try:
        output = os.system(cmd_Vdetailes)
        print("ffprobe runnning successfully")
    except:
        print("Error in ffprobe command")        

  
    vDetail_data = open('vdetail_log.txt','r')
    vDetailes_content = vDetail_data.readlines()
    
    list_detailvid = []
    for val_w in vDetailes_content:
        value = val_w.split("=")
        list_detailvid.append(value)
        
    
    frame_rate = list_detailvid[0]
    frame_rate = frame_rate[1]
    num,den = frame_rate.split( '/' )
    result_fps = int(float(num)/float(den))
    
    return result_fps
    

def gen_imos(original_content, bitrate_tocal_imos):
    imos_list_d = []
    
    fullfile_name = os.path.splitext(original_content)
    file_name = fullfile_name[0]
    mos_score_file = "frame_imos.txt"
    mos_outtext = "FrameLevel_imos.txt"

    qubit_cmd = "\"QubitInfo=ver1;stream-Id=" + file_name + ";sessionType=vod;fileName=" + mos_score_file + ";bitrate=" + bitrate_tocal_imos + ";mode=full;\""
    cmd_mos_score = "./mm_analyzer -qubitOpts "  + qubit_cmd  + " -i " + original_content + " -f null null.out"
    
    try:
        os.system(cmd_mos_score)
    except:
        print("Error in mm_analyzer command")
        
    cmd_mosdecode = "gcc base64QBRDecode_ver2.c"
    try:
        os.system(cmd_mosdecode)
    except:
        print("Error in gcc base64QBRDecode_ver2.c command")
        
    cmd_mosrun = "./a.out " + mos_score_file + " " + mos_outtext
    try:
        os.system(cmd_mosrun)
    except:
        print("Error in ./a.out command")
        
    mos_log = open('mosScore_outfile.txt', 'r')
    mos_log = mos_log.readlines()[1:]
    
    for mos_val in mos_log:
        val_c = mos_val.split(',')
        val_c = val_c[3]
        mos_data = val_c
        mos_data = float(mos_data)
        imos_list_d.append(mos_data)
        
    return imos_list_d
  
#Writing data into csv file
def gen_writecsv(imos_list_d, result_fps, path_tocsv, bitrate_kbps, original_content):
      
    with open(path_tocsv, 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(["TimeStamp",bitrate_kbps])
        
        frame_count = 1
        for imos_val in imos_list_d:
        
            timestamp = (frame_count * 1000) / result_fps
            csvwriter.writerow([timestamp,imos_val])
            frame_count = frame_count + 1
            
    csvfile.close()    
    

if __name__ == "__main__":

    if len(sys.argv) != 4:
        print("Usage of the script is as below")
        print("Usage ./gen_frameimos_concateCSVs.py transcoded_content AssetID Retain_mp4file(True or False)")
    else:
        original_content_url = sys.argv[1]
        asset_id = sys.argv[2]
        retain_file = sys.argv[3]
        
        current_wd = os.getcwd()
        
        try:
        
            path_todata = os.mkdir(current_wd + "/data")
        except:
            path_todata = current_wd + "/data"
        else:
            print("Successfully created the directory  ")
                
        content_link = original_content_url
        original_content = content_link.split('/')[-1]
        
        firstname = os.path.splitext(original_content)
        firstname_first = firstname[0]
        file_extension_url = firstname[1]
                
        if file_extension_url == '.m3u8':
            
            download_time = time.time()
            download_time = str(download_time)

            try:
            
                path_withTime_val = os.mkdir(path_todata + "/" + download_time)
                path_withTime = path_todata + "/" + download_time
                print("Creating your dicrectory ************************8")
            except:
                print("Directory creation failed to path_withTime")
            
            cmd_convertmp4 = "./youtube-dl -f mp4 --all-formats --restrict-filenames " + content_link + " -o " + path_withTime + "/" + "\"%(format)s.%(ext)s\""
            os.system(cmd_convertmp4)
            csv_frameLevel_list = []
            for hls_content_path in glob.glob(path_withTime + "/*.mp4"):
                original_content = hls_content_path.split('/')[-1]
                
                firstname = os.path.splitext(original_content)
                firstname_first = firstname[0]
                extract_filename = firstname_first.split(' ')
                bitrate_kbps = extract_filename[0] + "kbps"
                bitrate_formm = extract_filename[0]
                bitrate_formma = bitrate_formm.split('_')
                bitrate_tocal_imos = bitrate_formma[0]
                file_extension = firstname[1]
                first_file_name = firstname[0] + ".mp4"
                result_fps = gen_content_detailes(hls_content_path)
                csvfile_name = asset_id + '_' + firstname_first + '.csv'
                csv_frameLevel_list.append(csvfile_name)
                path_tocsv = path_withTime + "/" + csvfile_name
                imos_list_d = gen_imos(hls_content_path, bitrate_tocal_imos)   

                gen_writecsv(imos_list_d, result_fps, path_tocsv, bitrate_kbps, original_content)                
                
                
                if retain_file == "true":
                    os.remove(hls_content_path)
            
            outputCSV_file = asset_id + '.csv'
            csv_frameLevel_list_a = '"{}"'.format(str(csv_frameLevel_list))
            cmd_concate_csv = './concate_csvs.py ' + csv_frameLevel_list_a + ' ' + outputCSV_file + ' ' + path_withTime + ' ' + path_withTime
            try:
                os.system(cmd_concate_csv)
            except:
                print("Failed in cmd_concate_csv python file")