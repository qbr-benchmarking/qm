import csv
import os
import sys

def gen_gopLevel_imos(range_timestamp, range_profileData, seg_leg):
               
    weight_list = []
    imos_mul_wt_list = []
    
    for mos_data in range_profileData:
        
        mos_data = float(mos_data)
        
        weight = pow(2, -(mos_data)/0.35)
        weight_list.append(weight)
        
        imos_mul_wt = mos_data*weight
        imos_mul_wt_list.append(imos_mul_wt)                
    
    
    gop_imos_list = []    
    
    if len(weight_list) == len(imos_mul_wt_list):
        segment_count = 1
        all_timestamp = []
        for imos_gop in range(0, len(weight_list), seg_leg):
            wght_list = []
            imos_list = []
            
            
            
            for p in range(seg_leg-1):
                segment_countlist = []
                try:
                    wght_list.append(weight_list[imos_gop+p])
                    segment_countlist.append(range_timestamp[imos_gop+p])

                except:
                    print("catch in wght_list")
    
                try:
                    imos_list.append(imos_mul_wt_list[imos_gop+p])
                    
                except:
                    print("catch in imos_list")

            all_timestamp.append(segment_countlist[-1])
            imos_final = sum(imos_list)/ sum(wght_list)
            gop_imos_list.append(imos_final)
            

            
    return all_timestamp, gop_imos_list
            
def read_inputcsvfile(stime, etime, profile_name, inputcsvfile):
    with open(inputcsvfile, 'r', newline='') as readfile:
 
        reader = csv.DictReader(readfile)
        fieldnames = reader.fieldnames
        
        timestamp_list = []
        profile_data_list = []
        
        for row in reader:
            timestamp = row['Timestamp']
            profile_data = row[profile_name]
            timestamp_list.append(timestamp)
            profile_data_list.append(profile_data)
        
        range_timestamp = []
        range_profileData = []
        
        for a , b in zip(timestamp_list, profile_data_list):
            a = float(a)
            b = float(b)
            if a >= stime and a<= etime:
                
                range_timestamp.append(a)
                range_profileData.append(b)

    readfile.close()
    
    return range_timestamp, range_profileData
    
def write_outputcsvfile(all_timestamp, gop_imos_list, profile_name, outputcsv):
    with open(outputcsv, 'w',newline='') as writefile:
        csvwriter = csv.writer(writefile)
        
        csvwriter.writerow(["TImestamp",profile_name])
        
        for seg_time, imos_val in zip(segment_countlist,gop_imos_list):
        
            csvwriter.writerow([seg_time,imos_val])      
                      
    writefile.close()
        
if __name__ == "__main__":
    if len(sys.argv) != 7:
        print("Usage of the script is as below")
        print("Usage ./gop_levelIMOS.py startTime endTime segment_legth profile_name inputcsvfile1 outputcsvFile")
    else:
        stime = sys.argv[1]
        stime = float(stime)
        etime = sys.argv[2]
        etime = float(etime)
        seg_leg = sys.argv[3]
        seg_leg = int(seg_leg)
        profile_name = sys.argv[4]
        inputcsvfile = sys.argv[5]
        outputcsv = sys.argv[6]
        
        #Reading input csv file
        range_timestamp, range_profileData = read_inputcsvfile(stime, etime, profile_name, inputcsvfile)
        
        #Generating the GOP Level IMOS values 
        segment_countlist, gop_imos_list = gen_gopLevel_imos(range_timestamp, range_profileData, seg_leg)
        
        #Writing IMOS values to output csv file
        write_outputcsvfile(segment_countlist, gop_imos_list, profile_name, outputcsv)






