#!/usr/bin/python

import os
import sys
import csv
import glob
import ast

#Reading the CSV file
def read_csv(outcsvfile, pathToinputCSV_files, incsvfiles): 
    timestamp_alllist = []
    imos_alllist = []
    all_inone_dataList = []
    
    count = 1
    for each_csv_files in incsvfiles:
        for each_csv in glob.glob(pathToinputCSV_files + '/' + each_csv_files):
            eachTimestamp_list = []
            eachImos_list = []
            
            file = open(each_csv,'r')
            file_data = file.readlines()
    
            for row in file_data:
                rowone, rowtwo = row.split(',')
                rowtwo = rowtwo.strip()
                if count == 1:
                    eachTimestamp_list.append(rowone)
                eachImos_list.append(rowtwo)
            if count == 1:
                all_inone_dataList.append(eachTimestamp_list)
            all_inone_dataList.append(eachImos_list)
            count = count + 1
            file.close()
   
    return all_inone_dataList
    
#Writing to CSV file        
def write_csv(all_inone_dataList, pathTooutput_csv, outcsvfile):
    out_csv = pathTooutput_csv + '/' + outcsvfile
    with open(out_csv,'w') as writecsv:
        
        all_ele = []
        for ele in all_inone_dataList:
            for ele1 in ele:
                all_ele.append(ele1.strip())
            
        csv_write = csv.writer(writecsv)
        count_write = 0 
        for i in range(len(all_inone_dataList[0])):
            row_all = []
            for j in range(len(all_inone_dataList)):
                row_all.append(all_inone_dataList[j][i])
            csv_write.writerow(row_all)
        
        writecsv.close()        
               
if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage of the script is as below")
        print("Usage ./collate_csvs.py '[csvfile1,csvfile2,csvfile3,csvfile4]' outcsvfile pathToinputCSV_files pathTooutput_csv")
    else:
        incsvfiles = sys.argv[1]
        outcsvfile = sys.argv[2]
        pathToinputCSV_files = sys.argv[3]
        pathTooutput_csv = sys.argv[4]
        
        incsvfiles = ast.literal_eval(incsvfiles)

        #Reading the CSV file
        all_inone_dataList = read_csv(outcsvfile, pathToinputCSV_files, incsvfiles)

        #Writing to CSV file
        write_csv(all_inone_dataList, pathTooutput_csv, outcsvfile)
