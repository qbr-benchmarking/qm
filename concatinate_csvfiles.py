import os
import sys
import csv

def read_csv(outcsvfile, incsvfiles): 
    timestamp_alllist = []
    imos_alllist = []
    all_inone_dataList = []
    for each_csv in incsvfiles:
        eachTimestamp_list = []
        eachImos_list = []
        
        file = open(each_csv,'r')
        file_data = file.readlines()

        for row in file_data:
            rowone, rowtwo = row.split(',')
            rowtwo = rowtwo.strip()
            eachTimestamp_list.append(rowone)
            eachImos_list.append(rowtwo)
        all_inone_dataList.append(eachTimestamp_list)
        all_inone_dataList.append(eachImos_list)
        file.close()

    return all_inone_dataList
    
           
def write_csv(all_inone_dataList, outcsvfile):
    with open(outcsvfile, 'w', newline='') as writecsv:
        
        all_ele = []
        for ele in all_inone_dataList:
            for ele1 in ele:
                all_ele.append(ele1.strip())
            
        
        csv_write = csv.writer(writecsv)

        print(all_ele[0])
        count_write = 0 
        for i in range(len(all_inone_dataList[0])):
            row_all = []
            for j in range(len(all_inone_dataList)):
                row_all.append(all_inone_dataList[j][i])
                print(row_all)
            csv_write.writerow(row_all)
           

        writecsv.close()        
               
if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage of the script is as below")
        print("Usage ./concate_csvs.py csvfile1,csvfile2,csvfile3,csvfile4 outcsvfile")
    else:
        
        incsvfiles = sys.argv[1].split(',')
        outcsvfile = sys.argv[2]
        
        #Reading the CSV file
        all_inone_dataList = read_csv(outcsvfile, incsvfiles)

        #Writing to CSV file
        write_csv(all_inone_dataList, outcsvfile)