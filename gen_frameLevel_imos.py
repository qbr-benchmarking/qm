import os
import sys
import csv
import subprocess
import re

def gen_content_detailes():
    #Get detailes of original content using ffprobe
    cmd_Vdetailes = "ffprobe -v error -select_streams v:0 -show_entries stream=r_frame_rate -of default=noprint_wrappers=1 " + original_content + '> vdetail_log.txt'
    try:
        output = os.system(cmd_Vdetailes)
        print("runnning successfully")
    except:
        print("Error in ffprobe command")        

  
    vDetail_data = open('vdetail_log.txt','r')
    vDetailes_content = vDetail_data.readlines()
    
    list_detailvid = []
    for val_w in vDetailes_content:
        value = val_w.split("=")
        list_detailvid.append(value)
        
    
    frame_rate = list_detailvid[0]
    frame_rate = frame_rate[1]
    num,den = frame_rate.split( '/' )
    result_fps = int(float(num)/float(den))
    
    return result_fps
    

def gen_imos(original_content, bitrate_kbps):
    imos_list_d = []
    
    fullfile_name = os.path.splitext(original_content)
    file_name = fullfile_name[0]
    mos_score_file = "frame_imos.txt"
    mos_outtext = "FrameLevel_imos.txt"

    qubit_cmd = "\"QubitInfo=ver1;stream-Id=" + file_name + ";sessionType=vod;fileName=" + mos_score_file + ";bitrate=" + bitrate_kbps + ";mode=full;\""
    cmd_mos_score = "./mm_analyzer -qubitOpts "  + qubit_cmd  + " -i " + original_content + " -f null null.out"
    
    try:
        os.system(cmd_mos_score)
    except:
        print("Error in mm_analyzer command")
        
    cmd_mosdecode = "gcc base64QBRDecode_ver2.c"
    try:
        os.system(cmd_mosdecode)
    except:
        print("Error in gcc base64QBRDecode_ver2.c command")
        
    cmd_mosrun = "./a.out " + mos_score_file + " " + mos_outtext
    try:
        os.system(cmd_mosrun)
    except:
        print("Error in ./a.out command")
        
    mos_log = open('mosScore_outfile.txt', 'r')
    mos_log = mos_log.readlines()[1:]
    
    for mos_val in mos_log:
        val_c = mos_val.split(',')
        val_c = val_c[3]
        mos_data = val_c
        mos_data = float(mos_data)
        imos_list_d.append(mos_data)
        
    return imos_list_d
  

def gen_writecsv(imos_list_d, result_fps, csvfile_name, bitrate, original_content): 
    with open(csvfile_name, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(["TimeStamp",bitrate])
        
        frame_count = 1
        for imos_val in imos_list_d:
        
            timestamp = (frame_count * 1000) / result_fps
            csvwriter.writerow([timestamp,imos_val])
            frame_count = frame_count + 1
            
    csvfile.close()    
    
if __name__ == "__main__":

    if len(sys.argv) != 4:
        print("Usage of the script is as below")
        print("Usage ./qbr_bechmaking.py transcoded_content Bitrate(Mbps) imos")
    else:
        original_content = sys.argv[1]
        bitrate = sys.argv[2]
        bitrate_kbps = bitrate * 1000
        bitrate_kbps = bitrate_kbps + 'kbps'
        quality_matrice_list = sys.argv[3]
        
        
        firstname = os.path.splitext(original_content)
        firstname = firstname[0]
        csvfile_name = firstname + bitrate + '.csv'
        
        #It will fetch content detailes like fps etc
        result_fps = gen_content_detailes()
        
        #Generate imos scores using our mm_analyzer
        imos_list_d = gen_imos(original_content, bitrate_kbps)   
        
        #Writing imos to csv file
        gen_writecsv(imos_list_d, result_fps, csvfile_name, bitrate, original_content)