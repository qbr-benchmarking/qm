#!/usr/bin/python

import os
import sys
import csv
import subprocess
import re
import urllib
import requests
import time
import glob
import collate_csvs
import wget
from csv import writer

#Get detailes of transcoded content using ffprobe
def gen_content_detailes(original_content):
    fps_file = "vdetail_log.txt"
    transcoded_content = original_content.strip()
    cmd_Vdetailes = "./ffprobe -v error -select_streams v:0 -show_entries stream=r_frame_rate,duration -of default=noprint_wrappers=1 " + transcoded_content + ' > ' + fps_file
    
    try:
        output = os.system(cmd_Vdetailes)
        print("ffprobe command runnning successfully")
    except:
        print("Failed while running ffprobe command")        

    vDetail_data = open(fps_file,'r')
    vDetailes_content = vDetail_data.readlines()
    
    list_detailvid = []
    count_read = 1
    for val_w in vDetailes_content:
        value = val_w.split("=")
        if count_read == 1:
            value = val_w.split("=")
            list_detailvid.append(value)
        if count_read == 2:
            value = val_w.split("=")
            duration_sec = value[1]
            duration_sec = float(duration_sec)
        count_read = count_read + 1
       
    frame_rate = list_detailvid[0]
    frame_rate = frame_rate[1]
    num,den = frame_rate.split( '/' )
    result_fps = int(float(num)/float(den))
    
    os.remove(fps_file)
    return result_fps, duration_sec
    
#get frame level imos scores
def gen_imos(original_content, bitrate_tocal_imos, asset_id):
    imos_list_d = []
    
    fullfile_name = os.path.splitext(original_content)
    file_name = fullfile_name[0]
    mos_score_file = "frame_imos.txt"
    mos_outtext = "FrameLevel_imos.txt"

    qubit_cmd = "\"QubitInfo=ver1;stream-Id=" + asset_id + ";sessionType=vod;fileName=" + mos_score_file + ";bitrate=" + bitrate_tocal_imos + ";mode=full;\""
    cmd_mos_score = "./mm_analyzer -qubitOpts "  + qubit_cmd  + " -i " + original_content + " -f null null.out"
    
    try:
        os.system(cmd_mos_score)
    except:
        print("Failed while running mm_analyzer command")
                
    cmd_mosrun = "./mm_imosdecode " + mos_score_file +  " " + mos_outtext
    try:
        os.system(cmd_mosrun)
    except:
        print("Failed while running ./mm_imosdecode command")
        
    mos_log = open(mos_outtext, 'r')
    mos_data_log = mos_log.readlines()[1:]
    
    for mos_val in mos_data_log:
        val_c = mos_val.split(',')
        val_c = val_c[3]
        mos_data = val_c
        mos_data = float(mos_data)
        imos_list_d.append(mos_data)
    mos_log.close()

    os.remove(mos_score_file)
    os.remove(mos_outtext)

    return imos_list_d

#writing imos scores into csv file
def gen_writecsv(imos_list_d, result_fps, path_tocsv, header_name):
    
    with open(path_tocsv, 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(["TimeStamp",header_name])
        
        frame_count = 1
        for imos_val in imos_list_d:
        
            timestamp = (frame_count * 1000) / result_fps
            csvwriter.writerow([timestamp,imos_val])
            frame_count = frame_count + 1
            
    csvfile.close()    
    
if __name__ == "__main__":

    if len(sys.argv) > 5:
        print("Usage of the script\n")
        
        print("Usage ./gen_frameimos.py transcoded_content AssetID Retain_mp4file(True or False) mp4(this is optional only for mp4 contents))\n")
        print("If the content is mp4 then in place of transcoded_content pass readme.txt and mp4 as the last argument\n")
    else:
        length_a = len(sys.argv)
        
        original_content_url = sys.argv[1]
        asset_id = sys.argv[2]
        mp4_ext = None
        retain_file = sys.argv[3]
        if length_a == 5:
            original_content_url = sys.argv[1]
            mp4_ext = sys.argv[4]
   
        current_wd = os.getcwd()
        test_process = "status.csv"
        
        if os.path.isfile(test_process):
            print("Status file is present will append the data")
        else:
            list_header = ["AssetID","URL","Timestamp","No_of_profiles","Duration_sec","FPS","CSV_filename","Size_csvfile","Test_status"]
            with open(test_process, 'w') as assetidcsv:
                csvwriter = writer(assetidcsv)
                csvwriter.writerow(list_header)
            assetidcsv.close()

        if mp4_ext:
            mp4_details = original_content_url
            mp4_data = open(mp4_details, 'r')
            mp4_datalog = mp4_data.readlines()

            pwd_mp4 = current_wd + "/"
            path_todata = os.path.join(pwd_mp4, "data")

            try:
                os.mkdir(path_todata)
                path_todata_a = path_todata
            except:
                path_todata_a = path_todata
                print("Failed to create data dicrectory ")
            else:
                 print("data directory creation is successful")
  
            csv_files_list = []
            for mp4file in mp4_datalog:
                data_a = mp4file.split(' ')
                mp4url = data_a[0]
                mp4bitrate = data_a[1]
                mp4bitrate = mp4bitrate.strip()
                mp4bitrate_a = mp4bitrate + 'kbps'

                content_name = mp4url.split('/')[-1]

                filename_mp4 = os.path.splitext(content_name)
                filenamemp4 = filename_mp4[0]
                mp4_extension = filename_mp4[1]

                pathto_downloadmp4 = path_todata_a + '/' + content_name

                csvfile_name = asset_id + '_' + filenamemp4 + '.csv'
                csv_files_list.append(csvfile_name)

                path_tocsv = path_todata_a + "/" + csvfile_name
                wget.download(mp4url, pathto_downloadmp4)

                result_fps, duration_sec = gen_content_detailes(pathto_downloadmp4)

                imos_list_d = gen_imos(pathto_downloadmp4, mp4bitrate, asset_id)                
                gen_writecsv(imos_list_d, result_fps, path_tocsv, mp4bitrate)

                outputCSV_file = asset_id + '.csv'

                if retain_file == "false":
                    os.remove(pathto_downloadmp4)

            csv_frameLevel_list_mp4 = '"{}"'.format(str(csv_files_list))
            lengthof_profiles = len(csv_files_list)
            cmd_concate_csv = './collate_csvs.py ' + csv_frameLevel_list_mp4 + ' ' + outputCSV_file + ' ' + path_todata_a + ' ' + path_todata_a
            download_time = time.time()
            try:
                os.system(cmd_concate_csv)
            except:
                print("Failed while running collate_csvs.py")
                test_status = "fail"
            else:
                test_status = "pass"
                
            for inter_csv in csv_files_list:
                for csv_filedel in glob.glob(path_todata_a + '/' + inter_csv):
                    os.remove(csv_filedel)

            for csvfile_size in glob.glob(path_todata_a + '/' + outputCSV_file):
                filesize = os.path.getsize(csvfile_size)
                
            with open(test_process, 'a') as assetidcsv:
                csvwriter = writer(assetidcsv)
                csvwriter.writerow([asset_id,mp4_ext,download_time,lengthof_profiles,duration_sec,result_fps,outputCSV_file,filesize,test_status])
            assetidcsv.close()       
            
        else:
            pwd_hd = current_wd + "/"
            path_todata = os.path.join(pwd_hd, "data")

            try:
                os.mkdir(path_todata)
                path_todata = path_todata
            except:
                path_todata = path_todata
            else:
                print("data directory creation is successful ")

            download_time = time.time()
            download_time = str(download_time)

            content_link = original_content_url
            original_content = content_link.split('/')[-1]

            firstname = os.path.splitext(original_content)
            firstname_first = firstname[0]
            file_extension_url = firstname[1]

            if file_extension_url == '.m3u8':
                cmd_convertmp4 = "./youtube-dl -f mp4 --all-formats --restrict-filenames " + content_link + " -o " + path_todata + "/" + "\"%(format)s.%(ext)s\""
                os.system(cmd_convertmp4)
                csv_frameLevel_list = []
                for hls_content_path in glob.glob(path_todata + "/*.mp4"):
                    original_content = hls_content_path.split('/')[-1]

                    firstname = os.path.splitext(original_content)
                    firstname_first = firstname[0]
                    extract_filename = firstname_first.split(' ')
                    bitrate_kbps = extract_filename[0] + "kbps"
                    bitrate_formm = extract_filename[0]
                    bitrate_formma = bitrate_formm.split('_')
                    bitrate_tocal_imos = bitrate_formma[0]
                    file_extension = firstname[1]
                    first_file_name = firstname[0] + ".mp4"
                    csvfile_name = asset_id + '_' + firstname_first + '.csv'
                    result_fps, duration_sec = gen_content_detailes(hls_content_path)
                    
                    csv_frameLevel_list.append(csvfile_name)
                    path_tocsv = path_todata + "/" + csvfile_name
                    imos_list_d = gen_imos(hls_content_path, bitrate_tocal_imos, asset_id)   

                    gen_writecsv(imos_list_d, result_fps, path_tocsv, bitrate_tocal_imos)                

                    if retain_file == "false":
                        os.remove(hls_content_path)

                outputCSV_file = asset_id + '.csv'
                csv_frameLevel_list_a = '"{}"'.format(str(csv_frameLevel_list))
                lengthof_profiles = len(csv_frameLevel_list)
                cmd_concate_csv = './collate_csvs.py ' + csv_frameLevel_list_a + ' ' + outputCSV_file + ' ' + path_todata + ' ' + path_todata
                try:
                    os.system(cmd_concate_csv)
                except:
                    print("Failed while running collate_csvs.py")
                    test_status = "fail"
                else:
                    test_status = "pass"
                
                for inter_csv in csv_frameLevel_list:
                    for csvfile_del in glob.glob(path_todata + '/' + inter_csv):
                        os.remove(csvfile_del)

                for csvfile_size in glob.glob(path_todata + '/' + outputCSV_file):
                    filesize = os.path.getsize(csvfile_size)

                with open(test_process, 'a') as assetidcsv:
                    csvwriter = writer(assetidcsv)
                    csvwriter.writerow([asset_id,original_content_url,download_time,lengthof_profiles,duration_sec,result_fps,outputCSV_file,filesize,test_status])
                assetidcsv.close()  

            if file_extension_url == '.mpd':
                cmd_convertmp4 = "./youtube-dl -f mp4 --all-formats --restrict-filenames " + content_link + " -o " + path_todata + "/" + "\"%(url)s-%(width)sx%(height)s.%(ext)s\""
                os.system(cmd_convertmp4)
                csv_frameLevel_list = []
                for dash_content_path in glob.glob(path_todata + "/*.mp4"):
                    original_content = dash_content_path.split('/')[-1]
                    firstname = os.path.splitext(original_content)
                    firstname_first = firstname[0]
                    extract_filename = firstname_first.split('_')
                    bitrate_mpd = extract_filename[-2]
                    to_header = bitrate_mpd[:-1]
                    bitrate_kbps = extract_filename[-2] + "bps"
                    res = extract_filename[-1].split('-')
                    resolution = res[1]
                    header_name = bitrate_kbps + '_' + resolution
                    bitrate_formm = extract_filename[0]
                    bitrate_formma = bitrate_formm.split('-')
                    header_nameCSV = bitrate_formma[-1]
                    bitrate_tocal_imos = bitrate_kbps + '_' + header_nameCSV

                    result_fps, duration_sec = gen_content_detailes(dash_content_path)
                    csvfile_name = asset_id + '_' + header_name + '.csv'
                    csv_frameLevel_list.append(csvfile_name)
                    path_tocsv = path_todata + "/" + csvfile_name
                    imos_list_d = gen_imos(dash_content_path, bitrate_kbps, asset_id)   

                    gen_writecsv(imos_list_d, result_fps, path_tocsv, to_header)                

                    if retain_file == "false":
                        os.remove(dash_content_path)

                for audio_file in glob.glob(path_todata + "/*.m4a"):
                    os.remove(audio_file)

                outputCSV_file = asset_id + '.csv'
                csv_frameLevel_list_a = '"{}"'.format(str(csv_frameLevel_list))
                lengthof_profiles = len(csv_frameLevel_list)
                cmd_concate_csv = './collate_csvs.py ' + csv_frameLevel_list_a + ' ' + outputCSV_file + ' ' + path_todata + ' ' + path_todata
                try:
                    os.system(cmd_concate_csv)   
                except:
                    print("Failed while running collate_csvs.py")
                    test_status = "fail"
                else:
                    test_status = "pass"

                for inter_csv in csv_frameLevel_list:
                    for csv_del in glob.glob(path_todata + '/' + inter_csv):
                        os.remove(csv_del)

                for csvfile_size in glob.glob(path_todata + '/' + outputCSV_file):
                    filesize = os.path.getsize(csvfile_size)

                with open(test_process, 'a') as assetidcsv:
                    csvwriter = writer(assetidcsv)
                    csvwriter.writerow([asset_id,original_content_url,download_time,lengthof_profiles,duration_sec,result_fps,outputCSV_file,filesize,test_status])
                assetidcsv.close()  
