#!/usr/bin/python

import csv
import os
import sys

def gen_gopLevel_imos(range_timestamp, range_profileData, seg_leg, stime, etime):              
    weight_list = []
    imos_mul_wt_list = []
    
    for mos_data in range_profileData:
        
        mos_data = float(mos_data)
        
        weight = pow(2, -(mos_data)/0.35)
        weight_list.append(weight)
        
        imos_mul_wt = mos_data*weight
        imos_mul_wt_list.append(imos_mul_wt) 

    gop_imos_list = []
    all_timestamp = []
    time_idx = 0
    while(time_idx < len(range_timestamp)):
        segment_countlist = []
        wght_list = []
        imos_list = []
        
        while(time_idx < len(range_timestamp) and range_timestamp[time_idx] < stime+seg_leg):
            try:
                wght_list.append(weight_list[time_idx])
                imos_list.append(imos_mul_wt_list[time_idx])
                segment_countlist.append(range_timestamp[time_idx])
                time_idx = time_idx + 1
            except:
                print("catching exception")
     
        imos_final = sum(imos_list)/ sum(wght_list)
        gop_imos_list.append(imos_final)
        all_timestamp.append(segment_countlist[-1])
        stime = stime+seg_leg
 
    return all_timestamp, gop_imos_list
                  
def read_inputcsvfile(stime, etime, profile_name, inputcsvfile):
    with open(inputcsvfile, 'r') as readfile:
 
        reader = csv.DictReader(readfile)
        fieldnames = reader.fieldnames
        
        timestamp_list = []
        profile_data_list = []
        
        for row in reader:
            timestamp = row['TimeStamp']
            profile_data = row[profile_name]
            timestamp_list.append(timestamp)
            profile_data_list.append(profile_data)
        
        range_timestamp = []
        range_profileData = []
        
        for a , b in zip(timestamp_list, profile_data_list):
            a = float(a)
            b = float(b)
            if a >= stime and a<= etime:
                
                range_timestamp.append(a)
                range_profileData.append(b)

    readfile.close()
    
    return range_timestamp, range_profileData
    
def write_outputcsvfile(all_timestamp, gop_imos_list, profile_name, outputcsv):
    with open(outputcsv, 'w') as writefile:
        csvwriter = csv.writer(writefile)
        
        csvwriter.writerow(["TimeStamp",profile_name])
        
        for seg_time, imos_val in zip(all_timestamp,gop_imos_list):
        
            csvwriter.writerow([seg_time,imos_val])      
                      
    writefile.close()
        
if __name__ == "__main__":
    if len(sys.argv) != 7:
        print("Usage of the script is as below")
        print("Usage ./gop_levelIMOS.py startTime endTime segment_legth profile_name path_inputcsvfile1 path_outputcsvFile")
    else:
        stime = sys.argv[1]
        stime = float(stime)
        etime = sys.argv[2]
        etime = float(etime)
        seg_leg = sys.argv[3]
        seg_leg = int(seg_leg)
        profile_name = sys.argv[4]
        inputcsvfile = sys.argv[5]
        outputcsv = sys.argv[6]
        
        #Reading input csv file
        range_timestamp, range_profileData = read_inputcsvfile(stime, etime, profile_name, inputcsvfile)
        
        #Generating the GOP Level IMOS values 
        all_timestamp, gop_imos_list = gen_gopLevel_imos(range_timestamp, range_profileData, seg_leg, stime, etime)
        
        #Writing IMOS values to output csv file
        write_outputcsvfile(all_timestamp, gop_imos_list, profile_name, outputcsv)







